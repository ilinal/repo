var maxIndex = -1, max, iNul;
var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 100];
// 1.Вывести список в следующем виде:
console.log('Список студентов:');
for (i = 0; i < studentsAndPoints.length; i=i+2) {
    console.log('Студент ' + studentsAndPoints[i] + ' набрал ' + studentsAndPoints[i + 1] + ' баллов')
}
// 2.Найти студента набравшего наибольшее количество баллов, и вывести информацию в формате:
console.log('Студент набравший максимальный балл:');
for (var i = 1, imax = studentsAndPoints.length; i < imax; ++i) {
    if (maxIndex < 0 || studentsAndPoints[i] > max) {
        maxIndex = i;
        max = studentsAndPoints[i];
    }
}
console.log('Студент ' + studentsAndPoints[maxIndex - 1] + ' имеет максимальный балл ' + max);
//3. В группе появились новые студенты: «Николай Фролов» и «Олег Боровой». У них пока по 0 быллов. Добавить данные о них в массив.
studentsAndPoints.push('Николай Фролов', 0, 'Олег Боровой', 0);
console.log(studentsAndPoints);
//4. Студенты «Антон Павлович» и «Николай Фролов» набрали еще по 10 баллов, внести изменения в массив.
console.log(studentsAndPoints);
for (var i = 0, imax = studentsAndPoints.length; i < imax; i++) {
    if (studentsAndPoints[i] === 'Антон Павлович') {
        studentsAndPoints[i + 1] = studentsAndPoints[i + 1] + 10;
        console.log('У студента ' + studentsAndPoints[i] + ' теперь ' + studentsAndPoints[i + 1] + ' баллов');
    }
    if (studentsAndPoints[i] === 'Николай Фролов') {
        studentsAndPoints[i + 1] = studentsAndPoints[i + 1] + 10;
        console.log('У студента ' + studentsAndPoints[i] + ' теперь ' + studentsAndPoints[i + 1] + ' баллов');
    }
}
console.log(studentsAndPoints);
//5. Вывести список студентов, не набравших баллов:
console.log('Студенты не набравшие баллов:');
for (var i = 0, imax = studentsAndPoints.length; i < imax; i++) {
    if (studentsAndPoints[i] === 0) {
        console.log(studentsAndPoints[i - 1]);
    }
}
// Дополнительное задание: Удалить из массива данные о студентах, набравших 0 баллов.
for (var i = 0, imax = studentsAndPoints.length; i < imax; i++) {
    if (studentsAndPoints[i] === 0) {
        studentsAndPoints.splice(i - 1, 2);
    }
}
console.log(studentsAndPoints);
